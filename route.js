var url = require('url');

module.exports =
    function route(req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});

        var path = url.parse(req.url).pathname;
        switch(path) {
            case '/':
                res.end('Hello World!');
                break;
            case'/status':
                res.end('OK');
                break;
            default:
                res.writeHead(404);
                res.write('Page not found');
                res.end();
        }
    }
