var http = require('http');
var route = require('./route.js');

const app = http.createServer(route).listen(8080);

module.exports = app;
