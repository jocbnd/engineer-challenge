# Engineer Challenge

Cloud Engineer Challenge

## Running via Docker

Use Docker to install the engineer-challenge image.

```bash
docker pull registry.gitlab.com/jocbnd/engineer-challenge
```

Run the application in a Docker container. Specify a port number to direct traffic to on the host machine and include
the exposed port from the container (8080).

```bash
docker run -p 4000:8080 registry.gitlab.com/jocbnd/engineer-challenge
```

Visit the specified port at localhost:port to see the application in your browser.

## Running Locally

Install node.js. Once node.js is installed, install package dependencies.

```bash
npm install
```
Run the application using the following command:

```bash
node helloworld.js
```
Tests can be executed using:

```bash
npm test
```
